import low from 'lowdb';
import Memory from 'lowdb/adapters/Memory.js';
import Koa from 'koa';
import Router from '@koa/router';
import bodyParser from 'koa-bodyparser';
import { v4 as uuidv4 } from 'uuid';
import dotenv from 'dotenv';
import Encryption from './encryption.js';

dotenv.config();

const PORT = process.env.PORT || 4000;
const adapter = new Memory();
const db = low(adapter);
const app = new Koa();
const router = new Router();

db.defaults({ users: [], trips: [] }).write();

router.get('/', ctx => {
    ctx.body = '<h1>Hello Server!</h1>';
});

router.post('/users', async ctx => {
    const params = ctx.request.body;

    let alreadyRegistered = db
        .get('users')
        .find({ email: params.email })
        .value();

    if (alreadyRegistered) {
        ctx.body = {
            message: `User with email '${params.email}' is already registered`
        };
        ctx.status = 400;

        return ;
    }

    let password = await Encryption.cryptPassword(params.password);

    if (password) {
        db.get('users')
            .push({ id: uuidv4(), email: params.email, password: password })
            .write();

        ctx.body = { success: true };
    } else {
        ctx.status = 500;
        ctx.body = { message: 'Password encryption failed' };
    }
});

router.post('/login', async ctx => {
    const params = ctx.request.body;

    let user = db
        .get('users')
        .find({ email: params.email })
        .value();

    if (!user) {
        ctx.status = 404;
        ctx.body = {
            message: `User with email '${ctx.params.email}' is not found`
        };
        return;
    }

    let isPasswordMatch = await Encryption.comparePassword(params.password, user.password);

    if (!isPasswordMatch) {
        ctx.status = 400;
        ctx.body = { message: 'Wrong password input!' };
        return;
    }

    ctx.body = { id: user.id, email: user.email };
});

router.get('/trips/:id', ctx => {
    const trips = db
        .get('trips')
        .filter({ user: ctx.params.id })
        .value();

    ctx.body = trips || [];
});

router.post('/trips', ctx => {
    const params = ctx.request.body;
    const trips = db.get('trips');

    let alreadyBooked = trips
        .find({ trip: params.tripId, user: params.id })
        .value();

    if (alreadyBooked) {
        ctx.body = { message: `Trip '${params.tripId}' is already booked for user with id ${params.id}` };
        ctx.status = 400;
        return;
    }

    trips.push({ id: uuidv4(), user: params.id, trip: params.tripId })
        .write();

    ctx.body = { success: true };
});

router.del('/trips/:id/:tripId', (ctx) => {
    const trips = db.get('trips');

    let bookedTrip = trips
        .find({ user: ctx.params.id, trip: ctx.params.tripId })
        .value();

    if (bookedTrip) {
        trips.remove({ user: ctx.params.id, trip: ctx.params.tripId }).write();
        ctx.body = { success: true };
    } else {
        ctx.body = { message: `Trip '${ctx.params.tripId}' for user with id '${ctx.params.id}' is not found` };
        ctx.status = 404;
    }
});

app.use(bodyParser());
app.use(router.routes());
app.use(router.allowedMethods());
app.listen(PORT, () => {
    console.log(`Server is up and listen for queries at http://localhost:${PORT}`)
});
